# DesafioFlex

Este é o repositório referente ao desafio da **_Flex Relacionamentos Inteligentes_** para a vaga de desenvolvedor _fullstack_.

Em suma o objetivo era desenvolver uma [**RESTful API**](https://en.wikipedia.org/wiki/Representational_state_transfer) e uma frontend que conecte a ela para gerenciar um banco de dados de devedores.

## Linguagens utilizadas
Banco de dados:

* MySQL

Backend:

* Python
    + virtualenv
    + FLASK
        - Flask-RESTful
        - Flask_Cors
        - Flask-SQLAlchemy
        - Flask-MySQLdb
        - Flask-Jsonpify
        - SQLAlchemy
    - SQLAlchemy

Frontend:

* HTML/CSS
* jQuery
* Bootstrap

## Configuração

### Do virtualenv
Para um desenvolvimento e suposto lançamento mais limpo e seguro, foi optado criar um `virtualenv` e instalar apenas as bibliotecas necessárias. Para acessar o `virtualenv` incluso no repositório, basta apenas executar

>`$ source flex/bin/activate`

na raíz do repositório. 


### Do banco de dados
O serviço usa _exclusivamente_ bancos de dados em MySQL, então você terá que configurar um banco de dados e credenciais de acesso. Caso você já tenha **usuário**, **senha** e o **nome do banco de dados** em mãos, avance para a próxima seção, caso contrário, entre no `mysql` como `root` e crie o que é necessário

> 
```
    $ mysql -u root -p
    MySQL> CREATE DATABASE ${DB};
    MySQL> GRANT ALL PRIVILEGES on ${DB}.* TO '${user}'@$'{host}' IDENTIFIED BY '${password};
    MySQL> FLUSH PRIVILEGES ${DB};
    MySQL> BYE;
```

### Do servidor
Para iniciar o servidor, entre primeiramente no diretório `Flask` e execute a aplicação

> `$ cd Flask` \
> `$ ./server.py`

Execute o script e siga as orientações de primeira configuração. Primeiramente será verificado se existe o arquivo de configurações `settings.json` no diretório `Flask`. Como não há, o script criará um com as configurações padrões e, por padrão, acesso apenas para o dono e grupo do arquivo (`640`), pois ele contém informações sensíveis.

Edite o arquivo `settings.json` criado e coloque as credenciais do `MySQL` assim como um nome de tabela que será utilizado.

Execute novamente o script e será perguntado se você gostaria de criar uma tabela com os campos padrão que a `API` irá utilizar. Diga que sim e o servidor estará rodando num servidor de desenvolvimento (`Werkzeug`).

### Do frontend
Para configurar o website, basta colocá-lo num caminho de algum servidor HTML (`apache`, `nginx`, etc). Tudo que é necessário já está no próprio root da pasta `website`.
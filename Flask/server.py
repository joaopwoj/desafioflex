#! /usr/bin/env python
from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_restful.utils import cors
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask_jsonpify import jsonpify
from flask_cors import CORS
import json
import datetime

__version__ = 'v1.0'
isDebug = True

def debug(message):
    '''
    Utility function to print debug info.
    '''
    global isDebug
    if isDebug:
        print(":: DEBUG :: {}".format(str(message)))

def defaultConfigs():
    return { "SQLcredentials":
             {
                "username": "default",
                "password": "pass"
             },
             "SQLconfig":
             {
                "host": "127.0.0.1",
                "database": "default",
                "table": "debtors"
             },
             "server": {
                 "IP": "127.0.0.1",
                 "port": 5003
             }
           }

def checkInput(inputString):
    '''
    Check if 'string' does not contain special chars, i.e., it is SQL safe.
    '''
    import string
    invalidChars = set(string.punctuation)

    return any(char in invalidChars for char in inputString)

def loadConfig(settingsFile="settings.json"):
    '''
    Check for a configuration file containing all the credentials of the server and
    database.
    '''
    from os.path import isfile
    from os import chmod
    from sys import exit
    import stat

    if isfile(settingsFile):
        with open(settingsFile, 'r') as file:
            try:
                settings = json.load(file)
            except:
                print("Couldn't open '{}' file.".format(settingsFile))
                exit(-1)
    else:
        # Default settings
        settings = defaultConfigs()
        print("Settings file not found. Writing default into '{}'".format(settingsFile))
        with open(settingsFile, 'w') as f:
            json.dump(settings,f)

        # Only the owner show be able to read/write on this file
        chmod(settingsFile, stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP)
        print("Please edit '{}' to configure your MySQL credentials.".format(settingsFile))
        exit(2)

    table = settings['SQLconfig']['table']
    if checkInput(table):
        print("Invalid SQL-friendly field name: {}".format(table))
        exit(-1)

    return settings

def setupTables(db, table='debtors'):
    '''
    Makes sure the debtors' table exists. If True, do nothing, otherwise create it with
    the expected fields.
    '''
    from sys import exit
    # fetchall() return tuples, so we need to make sure (table,) is in the DB
    if not (table,) in db.execute('SHOW TABLES;').fetchall():
        answer = input("Table {} not found. Would you like to create it? [y]/n: ".format(table))
        if answer in ['y', 'Y', '']:
            db.execute("CREATE TABLE {} ( \
                    debtID INT NOT NULL, \
                    userID INT NOT NULL, \
                    name VARCHAR(64) NOT NULL, \
                    reason VARCHAR(255), \
                    date DATE NOT NULL, \
                    value DOUBLE(12,2) NOT NULL, \
                    PRIMARY KEY(debtID) \
                    )".format(''.join(table)))
        else:
            print("Please edit the settings file to setup the correct MySQL table.")
            exit(2)
    debug("Table '{}' found.".format(table))




app = Flask(__name__)
api = Api(app)
cors = CORS(app)

settings = loadConfig()
db = create_engine("mysql://{}:{}@{}/{}".format(settings['SQLcredentials']['username'],
                                                settings['SQLcredentials']['password'],
                                                settings['SQLconfig']['host'],
                                                settings['SQLconfig']['database']))
try:
    debug("Checking the credentials...")
    db.connect()
    Session = sessionmaker()
    Session.configure(bind=db)
    session = Session()
except:
    print("Couldn't connect to {}@{}".format(settings['SQLcredentials']['username'],
                                         settings['SQLcredentials']['host']))
    print("Please edit the settings file to configure your MySQL credentials.")
    exit(-1)

debug("Checking for the {} table...".format(settings['SQLconfig']['table']))
setupTables(session,table=settings['SQLconfig']['table'])

# Purge sensitive data
del(settings['SQLcredentials'])

# Argparse for POST/PUT requests
parser = reqparse.RequestParser()
parser.add_argument('userid', required=False, help="Unique UserID.",          type=int)
parser.add_argument('debtid', required=False, help="Unique DebtID.",          type=int)
parser.add_argument('name',   required=True,  help="Debtor's name.",          type=str)
parser.add_argument('reason', required=True,  help="The reason of the debt.", type=str)
parser.add_argument('date',   required=True,  help="When this debt was generated.",
                                                                              type=str)
parser.add_argument('value',  required=True,  help="The value of the debt.",  type=float)


class Utilities():
    def __init__(self, table):
        self.table = table
        self.conn = db.connect()

    def idExists(self,key,value):
        if self.conn.execute("SELECT {} FROM {} WHERE {}={};"\
                    .format(key, self.table, key, value)).fetchall():
            return True
        else:
            debug("Record '{}={}' does not exist.".format(key,value))
            return False

    def getNextID(self,key):
        if self.conn.execute("SELECT {} FROM {};".format(key,self.table)).fetchall():
            return self.conn.execute('SELECT MAX({}) FROM {};'.format(key,self.table)) \
                       .fetchone()[0] + 1
        else:
            return 1

    def getName(self,userid):
        if self.idExists('userID',userid):
            return self.conn.execute("SELECT userID FROM {} WHERE userID={};"
                                     .format(self.table,userid)).fetchone()[0]



class DebtorsList(Resource,Utilities):
    def __init__(self, table):
        Utilities.__init__(self,table)

    def get(self):
        query = self.conn.execute("SELECT DISTINCT(userID),name FROM {};"
                                  .format(self.table))
        result = [dict(zip(tuple(query.keys()),i)) for i in query.cursor]
        return {'data': result}

    def put(self):
        NotImplemented

    def post(self):
        args = parser.parse_args()
        self.conn.execute("INSERT INTO {}\
                           VALUES ('{}', '{}', '{}', '{}', '{}', '{}');"
                        .format(self.table,
                                self.getNextID('debtID'),
                                self.getNextID('userID'),
                                args['name'],
                                args['reason'],
                                args['date'],
                                args['value'],
                                ))
    def delete(self):
        NotImplemented


class Debtor(Resource,Utilities):
    def __init__(self, table):
        Utilities.__init__(self,table)

    def get(self,userid):
        if self.idExists('userID',userid):
            query = self.conn.execute("SELECT * FROM {} WHERE userID={};"
                                      .format(self.table,userid))
            result = [dict(zip(tuple(query.keys()),i)) for i in query.cursor]
            for row in result:
                row['date'] = row['date'].isoformat()
            return {'data': result}

    def put(self,userid):
        if self.idExists('userID',userid):
            try:
                args = request.get_json()
            except:
                args = parser.parse_args()
            debug(("JSON TEST", args))
            self.conn.execute("UPDATE {}\
                               SET name='{}'\
                               WHERE userID={};"
                            .format(self.table,
                                    args['name'],
                                    userid
                                    ))

    def post(self,userid):
        args = parser.parse_args()
        self.conn.execute("INSERT INTO {}\
                           VALUES ('{}', '{}', '{}', '{}', '{}', '{}');"
                        .format(self.table,
                                self.getNextID('debtID'),
                                userid,
                                args['name'],
                                args['reason'],
                                args['date'],
                                args['value'],
                                ))


    def delete(self,userid):
        if self.idExists('userID',userid):
            query = self.conn.execute("DELETE FROM {} WHERE userID={};"\
                                      .format(self.table, userid))


class Debt(Resource,Utilities):
    def __init__(self, table):
        Utilities.__init__(self,table)

    def get(self,debtid,userid):
        if self.idExists('userID',userid) and self.idExists('debtID',debtid):
            query = self.conn.execute("SELECT * FROM {} WHERE debtID={} AND userID={};"
                                      .format(self.table,debtid,userid))
            result = [dict(zip(tuple(query.keys()),i)) for i in query.cursor][0]
            result['date'] = result['date'].isoformat()
            return {'data': result}

    def put(self,debtid,userid):
        if self.idExists('userID',userid) and self.idExists('debtID',debtid):
            try:
                args = request.get_json()
            except:
                args = parser.parse_args()
            debug(("JSON TEST", args))
            self.conn.execute("UPDATE {}\
                               SET name='{}', reason='{}', \
                               date='{}', value='{}'\
                               WHERE debtID={} AND userID={};"
                            .format(self.table,
                                    args['name'],
                                    args['reason'],
                                    args['date'],
                                    args['value'],
                                    debtid,
                                    userid
                                    ))

        else:
            try:
                args = request.get_json()
            except:
                args = parser.parse_args()
            debug(("JSON TEST", args))
            self.conn.execute("INSERT INTO {}\
                               VALUES ('{}', '{}', '{}', '{}', '{}', '{}');"
                            .format(self.table,
                                    self.getNextID('debtID'),
                                    userid,
                                    args['name'],
                                    args['reason'],
                                    args['date'],
                                    args['value'],
                                    ))

    def post(self,debtid,userid):
        try:
            args = request.get_json()
        except:
            args = parser.parse_args()
        debug(("JSON TEST", args))
        self.conn.execute("INSERT INTO {}\
                           VALUES ('{}', '{}', '{}', '{}', '{}', '{}');"
                        .format(self.table,
                                self.getNextID('debtID'),
                                userid,
                                args['name'],
                                args['reason'],
                                args['date'],
                                args['value'],
                                ))

    def delete(self,debtid,userid):
        if self.idExists('userID',userid) and self.idExists('debtID',debtid):
            query = self.conn.execute("DELETE FROM {} WHERE debtID={} AND userID={};"\
                                      .format(self.table, debtid, userid))


api.add_resource(DebtorsList, '/{}/debtors'.format(__version__),
                 resource_class_kwargs={'table': settings['SQLconfig']['table']})
api.add_resource(Debtor, '/{}/debtors/<int:userid>'.format(__version__),
                 resource_class_kwargs={'table': settings['SQLconfig']['table']})
api.add_resource(Debt, '/{}/debtors/<int:userid>/debt/<int:debtid>'.format(__version__),
                 resource_class_kwargs={'table': settings['SQLconfig']['table']})



if __name__ == "__main__":
    port = settings['server']['port']
    app.run(port=port)

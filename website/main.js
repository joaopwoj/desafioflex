"use strict";

let host = 'http://127.0.0.1:5003/v1.0'

function closeUserDetails()
{
    $('#cntUserDetails').addClass("d-none");
    $('#cntDebtInfo').addClass("d-none");
}

function addUser()
{
    var newName = $("#fldMdlNewName").val();
    var newReason = $("#fldMdlNewReason").val();
    var newValue = $("#fldMdlNewValue").val();
    var newDate = $("#fldMdlNewDate").val();

    fetch(`${host}/debtors`,
    {
        method: 'POST',
        body: JSON.stringify({
            name:   newName,
            value:  newValue,
            date:   newDate,
            reason: newReason
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    })
    .then(setTimeout(refreshUserList,500));
    
    $("#mdlAddUser").modal('hide');
    closeUserDetails();
}

function removeUser()
{
    var userid = currentDebt.userID;
    
    if (userid != null) {
        fetch(`${host}/debtors/${currentDebt.userID}`,
            { method: 'DELETE' })
            .then(() => setTimeout(refreshUserList,500));
    }

    $("#mdlConfirmDeleteUser").modal('hide');
    closeUserDetails();    
}

function editUser()
{
    enableUserField();
    $("#spnUserEditButtons").addClass("d-none");
    $("#spnUserCommitButtons").removeClass("d-none");
}

function cancelEditUser()
{
    disableUserField();
    $("#spnUserEditButtons").removeClass("d-none");
    $("#spnUserCommitButtons").addClass("d-none");
}

function enableUserField()
{
    var field = $("#fldUserName");
    field.removeClass("form-control-plaintext");
    field.removeAttr("readonly");
    field.addClass("form-control");
}

function disableUserField()
{
    var field = $("#fldUserName");
    field.removeClass("form-control");
    field.attr("readonly",true);
    field.addClass("form-control-plaintext");
}

function saveUserField()
{
    var updatedName = $('#fldUserName').val();
    var userID = currentDebt.userID;
    
    if (userID != null)
    {
        fetch(`${host}/debtors/${userID}`,
            {
                method: 'PUT',
                body: JSON.stringify({
                    name: updatedName,
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(setTimeout(refreshUserList, 500))
            .then(currentDebt.refreshDebtList());
    } 
    cancelEditUser();
}

function activateUser(element)
{
    $('#cntUserDetails').removeClass('d-none');
    // Remove 'active' class from all elements
    $('a.debtor').removeClass('active');
    element.classList.add('active');

    var userID = element.getElementsByClassName('userid')[0].textContent;
    var name = element.getElementsByClassName('username')[0].textContent;

    // Set #cntUserInfo fields
    $('#fldUserName').val(name);
    $('#spnUserID').text(userID);
    currentDebt = new debt(userID, null, name, null, null, null);
    currentDebt.refreshDebtList();
}

function makeUserNode(id, name)
{
    return `\
            <a href='#' \
            onclick='activateUser(this)'
            class='list-group-item list-group-item-action debtor'>\
                <span class="username">${name}</span> <br>\
                <span class="userid text-black-50">${id}</span>\
            </a>`;
}

function refreshUserList()
{
    $('a.debtor').remove();
    $('a.debt').remove();
    fetch(`${host}/debtors`)
        .then(response => response.json())
        .then(json => {
            var data = json['data'];
            var userListId = $('#lstDebtors');
            for (var i = 0; i < data.length; i++) {
                var user = data[i];
                userListId.append(makeUserNode(user.userID, user.name));
            }
        });
}

let currentDebt = new debt();

function debt(userID, debtID, name, value, reason, date) 
{
    this.name = name;
    this.userID = userID;
    this.debtID = debtID;
    this.value = value;
    this.reason = reason;
    this.date = date;
    this.debtList = [];

    this.refreshDebtList = function ()
    {
        $('a.debt').remove();
        this.debtList = [];
        fetch(`${host}/debtors/${this.userID}`)
            .then(response => response.json())
            .then(json => { 
                var data = json['data'];
                var lstDebts = $('#lstDebts');
                for (var i = 0; i < data.length; i++)
                {
                    var instance = data[i];
                    this.debtList.push(instance);
                    lstDebts.append(this.makeNode(i, instance.debtID, instance.value));
                }
            });
    }

    this.makeNode = function(idx, id, value)
    {
        return `\
                <a href='#' \
                onclick='currentDebt.activate(this)'
                class='list-group-item list-group-item-action debt'>\
                    <span class="debtidx">${idx}</span>\
                    <span class="debtid">${id}</span> <br>\
                    <span class='value text-black-50'>R$${value.toFixed(2)}</span>\
                </a>`;
    }

    this.activate = function(element)
    {
        $('#cntDebtInfo').removeClass('d-none');
        // Remove 'active' class from all elements
        $('a.debt').removeClass('active');
        element.classList.add('active');

        var debtIDX = element.getElementsByClassName('debtidx')[0].textContent;

        var debt = this.debtList[debtIDX];
        this.debtID = debt.debtID;
        this.value = debt.value;
        this.reason = debt.reason;
        this.date = debt.date;

        this.refreshDebtInfo();
    }

    this.refreshDebtInfo = function ()
    {
        $('#fldReason').val(this.reason);
        $('#fldValue').val(this.value);
        $('#fldDate').val(this.date);
    }

    this.edit = function ()
    {
        this.enableFields();
        $("#cntEditButtons").addClass("d-none");
        $("#cntCommitButtons").removeClass("d-none");
    }

    this.cancel = function ()
    {
        this.disableFields();
        $("#cntEditButtons").removeClass("d-none");
        $("#cntCommitButtons").addClass("d-none");
    }

    this.enableFields = function ()
    {
        var fields = $("#cntDebtInfo").find("[id^=fld]");
        fields.removeClass("form-control-plaintext");
        fields.removeAttr("readonly");
        fields.addClass("form-control");
    }

    this.disableFields = function ()
    {
        var fields = $("#cntDebtInfo").find("[id^=fld]");
        fields.removeClass("form-control");
        fields.attr("readonly",true);
        fields.addClass("form-control-plaintext");
    }

    this.clearFields = function () 
    {
        $("#cntDebtInfo").find("[id^=fld]").val("");
    }

    this.save = function()
    {
        this.reason = $('#fldReason').val();
        this.value  = $('#fldValue').val();
        this.date   = $('#fldDate').val();

        if (this.debtID != null)
        {
            fetch(`${host}/debtors/${this.userID}/debt/${this.debtID}`,
                {
                    method: 'PUT',
                    body: JSON.stringify({
                        name:   this.name,
                        value:  this.value,
                        date:   this.date,
                        reason: this.reason
                    }),
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                })
                .then(setTimeout(this.refreshDebtList, 500));
        } else {
            fetch(`${host}/debtors/${this.userID}`,
                {
                    method: 'POST',
                    body: JSON.stringify({
                        name: this.name,
                        value: this.value,
                        date: this.date,
                        reason: this.reason
                    }),
                    headers: {
                        "Content-type": "application/json; charset=UTF-8"
                    }
                })
                .then(setTimeout(this.refreshDebtList, 500));
        }
        this.cancel();
    }

    this.add = function () 
    {
        var newReason = $("#fldMdlNewDebtReason").val();
        var newValue = $("#fldMdlNewDebtValue").val();
        var newDate = $("#fldMdlNewDebtDate").val();
    
        fetch(`${host}/debtors/${userID}`,
            {
                method: 'POST',
                body: JSON.stringify({
                    name: this.name,
                    value: newValue,
                    date: newDate,
                    reason: newReason
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(() => this.refreshDebtList());
        
        $("#mdlAddDebt").modal('hide');
        this.closeDebtInfo();
    }

    this.remove = function () 
    {
        fetch(`${host}/debtors/${this.userID}/debt/${this.debtID}`,
                { method: 'DELETE' })
                .then(() => this.refreshDebtList());
        this.closeDebtInfo();
    }

    this.closeDebtInfo = function()
    {
        $('#cntDebtInfo').addClass("d-none");
    }

}


window.onload = function () {
    $('[data-toggle="tooltip"]').tooltip();
    refreshUserList();
}
